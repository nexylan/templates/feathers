import getPort from 'get-port';
import logger from './logger';
import app from './app';

(async () => {
  const port = process.env.PORT
    ? process.env.PORT
    : await getPort({ port: getPort.makeRange(3000, 3100) });
  const server = app.listen(port);

  process.on('unhandledRejection', (reason, p) => logger.error('Unhandled Rejection at: Promise ', p, reason));
  server.on('listening', () => logger.info('Feathers application started on http://localhost:%d', port));
})();
