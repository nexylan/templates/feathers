import { Application as ExpressFeathers } from '@feathersjs/express';
import '@feathersjs/transport-commons';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ServiceTypes {}
export type Application = ExpressFeathers<ServiceTypes>;
