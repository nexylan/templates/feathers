all: install up

install:
	npm install

up:
	npm run dev

.PHONY: test
test:
	npm run test
