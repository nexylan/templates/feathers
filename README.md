# FeathersJS

- Demo: https://feathers.templates.nexylan.dev
- Official website: https://feathersjs.com/

## Requirements

- Nodes

## New project setup

Get the download link of the latest version from the [releases page](-/releases).

```bash
wget https://gitlab.com/nexylan/templates/feathers/-/archive/master/feathers-master.tar.gz
tar xf feathers-master.tar.gz
rm feathers-master.tar.gz
mv feathers-master my-project
cd my-project
git init .
```

Review `package.json` and `config/default.json` to match your needs.

## Usage

Run:

```
make
```

Then click on the given url at the end on the output.

And voilà, you are ready to rock. 🎸

## Deployment

See: https://gitlab.com/nexylan/templates/docs/blob/master/README.md
